<?php
namespace WEBLAB54\BookingComClient;

class Config
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return require 'config/app.php';
    }

    /**
     * @return mixed
     */
    public function getDevConfig()
    {
        return require 'config/app.php';
    }
}